slide 1: Provavel título do TCC, definido no primeiro encontro.
slide 2: Uma breve introdução a Visão computacional.
slide 3: Problemática 
		- Atualmente podemos perceber inumeros sistemas inteligentes que dispensam a necessidade do usuário
		interagir usando um meio físico, como mouse ou teclado, podemos ver a grande variedade de aplicações com apenas 
		comandos gestuais, mas esses ainda requerem que uma pessoa esteja perto de um computador, e se identificar.
		  Para que haja uma computação sem contato fisico, o primeiro passo é que o computador seja capaz de
		detectar uma pessoa que possa interagir com e sistema, e a reconhecer para saber suas permissões de acordo
		com o software utilizado.
		  Tendo essa identificação a partir de uma simples camera, uma gama possibilidade de novos meios
		de utulização de um computador seriam possíveis, dessa forma a interação com um amiente poderia ser
		feita de forma intrusiva em um ambiente inteligente.

slide 4: Objetivo
		- O objetivo deste trabalho é a implementação de um sistema capaz de reconhecer que há uma, ou mais, 
		pessoas em um ambiente e reconhecer qual pessoa é através de uma câmera comum, isso poderá ser feito com a
		detecção da face da pessoa, ou também por outros métodos, como um marcador (crachá).

		
slide 5: Revisão bibliográfica
		- Após uma busca pela string pré definida "people detection computer vision", e através de alguns artigos
		selecionados e classificados por importância para o projeto, foram selecionados artigos contendo técnicas,
		comaprativos de melhores técnicas, métodos e problemáticas de detecção e reconhecimento de pessoas. Artigos
		Que serão úteis para o alcance do objetivo.

slide 6:	- Também foram encontradas bibliotecas e APIS de reconhecimento de faces como betafaceapi(http://betafaceapi.com/),
		CCV(http://libccv.org/) e OpenBR(http://openbiometrics.org/).

slide 7: Justificativa
		- Olhando o problema vemos que seria de uma grande útilidade a possibilidade de detecção e identificação de
		pessoas em um ambiente através de uma simples câmera, abrindo uma gama de possibilidades como a de detecção
		de intrusos em locais proibidos, ou pensando também nessa linha, a procura de foragidos por câmeras sem
		precisar de alguém que fique procurando pelo monitor.
		  Alem disso, abre uma porta para um "smart house", onde basta um gesto, e você pode trancar e abrir portas,
		liga e desligar luzes, desligar eletrodomésticos e várias outras possibilidades.Qualquer tipo de iteração com 
		uma máquina, poderia ser feita dispensando periféricos.
		  Serão facilidades geradas como o controle de presença, saber quem esteve presente em determinado local em uma 
		determinada hora, fazer a chamada não seria mais um dever do professor, preparar um local para uso bastaria a sua
		aproximação.

slide 8: Métodologia
		- Para esse trabalho ser possível, a ideia atual seria a de com uma câmera simples tirando fotos e enviando para um
		programa que detectaria pessoas na foto, e segmentaria a imagem no local das faces das pessoas identificadas devolvendo
		apenas a foto das faces de pessoas da imagem, essas fotos seriam então enviadas a uma API de reconhecimento facila, essa
		API identificaria a pessoa, que após a sua identificação seria pego seus dados e exibidos para quem estivesse controlando.
		- O objetivo proposto será a junção de tecnicas de reconhecimentos e, atravéz de visão computacional,
		elaborar formas para que seja possível a implementação de sistemas capazes de reconhecer que há uma,
		ou mais, pessoas em um local e além de detectar essas pessoas identificá-las.

slide 9: Próximos passos
		- Nos passos seguintes, terei a definição de técnicas para o reconhecimento de pessoas.
		  Definição de métodos usados para detecção e segmentação de faces.
		  Encontrar API para reconhecimento de faces.
		
