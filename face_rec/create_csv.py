#!/usr/bin/env python

import sys
import os.path

if __name__ == "__main__":

    BASE_PATH="./new/"
    arq = open('images','w')

    for dirname, dirnames, filenames in os.walk(BASE_PATH):
        for filename in os.listdir(dirname):
            f = filename.split('-')
            abs_path = "%s/%s;%s" % (dirname, filename, f[0])
            line = "%s;%s;%s;%s;%s\n" % (abs_path, f[1], f[2], f[3], f[4].split('.')[0])
            arq.write(line)

    arq.close()
