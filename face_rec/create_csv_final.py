#!/usr/bin/env python

import sys
import os.path

if __name__ == "__main__":

    BASE_PATH="./modified/"
    arq = open('csv_final','w')
    
    for dirname, dirnames, filenames in os.walk(BASE_PATH):		
        for filename in os.listdir(dirname):
            abs_path = "%s%s;%s\n" % (dirname, filename, filename.split('-')[0])
            arq.write(abs_path)

    arq.close()
