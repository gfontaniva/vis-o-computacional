#include "opencv2/core/core.hpp"
#include "opencv2/contrib/contrib.hpp"
#include "opencv2/highgui/highgui.hpp"
#include "opencv2/imgproc/imgproc.hpp"
#include "opencv2/objdetect/objdetect.hpp"

#include <stdio.h>
#include <iostream>
#include <fstream>
#include <sstream>
#include <sys/time.h>

using namespace cv;
using namespace std;

void recognizePerson(string class_name);
bool generate_log(string nome, string class_name);
static void read_csv(const string& filename, vector<Mat>& images, vector<int>& labels, char separator);
static void read_class(const string& filename, vector<string>& nomes, vector<int>& ids, char separator);
void trainFaceRec(int id);
void addClass();
void detectAndDisplay( Mat frame, bool pic, int id );
string itos(int a);

String face_cascade_name = "haarcascade_frontalface_default.xml";
String eyes_cascade_name = "haarcascade_eye_tree_eyeglasses.xml";
CascadeClassifier face_cascade;
CascadeClassifier eyes_cascade;
String window_name = "Face detection";

int main(int argc, const char *argv[]) {
    int opt = 4, id;
    string class_name;
   
    while(opt) {
        cout << "1-Cadastrar pessoas\n2-Cadastrar aula\n3-Reconhecer\n0-Sair" << endl;
        cin >> opt;
    
        if(opt == 1) {
            cout << "ID: " << endl;
            cin >> id;
            trainFaceRec(id);

            system("python create_csv.py");
            system("python aligning.py");
            system("python create_csv_final.py");
        }
        if(opt == 2) addClass(); 
        if(opt == 3) {
            cout << "Class name: " << endl;
            cin >> class_name;
            recognizePerson(class_name);
        }
        destroyAllWindows();
    }
    return 0;
}


void recognizePerson(string class_name){   
    string fn_haar = "haarcascade_frontalface_alt.xml";
    string fn_csv = "csv_final";
    int deviceId = -1;
    bool log=false;
    
    vector<Mat> images;
    vector<int> labels;
    
    vector<string> nomes;
    vector<int> ids;
    
    try {
        read_class("class/"+class_name, nomes, ids, ';');
        read_csv(fn_csv, images, labels, ';');
    } catch (cv::Exception& e) {
        cerr << "Erro ao ler o arquivo: " << e.msg << endl;
        exit(1);
    }

    int im_width = images[0].cols;
    int im_height = images[0].rows;
    double diff;
    int label;
    Ptr<FaceRecognizer> model = createFisherFaceRecognizer();
    //int exibir = 0;

    model->set("threshold", 3000.0);
    model->train(images, labels);

    CascadeClassifier haar_cascade;
    haar_cascade.load(fn_haar);
    
    VideoCapture cap(deviceId);
    if(!cap.isOpened()) {
        cerr << "Capture Device ID " << deviceId << "cannot be opened." << endl;
        return;
    }
    
    Mat frame;
    //int x;
    //for(x=0;x<19;x++) {       
    for(;;) {       
        //int seconds, useconds, mtime;
        //struct timeval start, end;
        //gettimeofday(&start, NULL);

        cap >> frame;
        Mat original = frame.clone();

        //Mat original = imread("foto"+itos(x)+".jpg", CV_LOAD_IMAGE_COLOR);   // Read the file

        Mat gray;
        cvtColor(original, gray, CV_BGR2GRAY);

        vector< Rect_<int> > faces;
        haar_cascade.detectMultiScale(gray, faces, 1.2);
        
        char key = (char) waitKey(20);
        if(key == 27) break;
        if(key == 32) log = !log;
        

        for(unsigned int i = 0; i < faces.size(); i++) {
            Rect face_i = faces[i];      
              
            // Crop the face from the image. So simple with OpenCV C++:
            Mat face = gray(face_i);
            Mat face_resized;
            cv::resize(face, face_resized, Size(im_width, im_height), 1.0, 1.0, INTER_CUBIC);
            
            //equalizeHist( face_resized, face_resized );

            //int prediction = model->predict(face_resized);
            model->predict(face_resized, label, diff);

            rectangle(original, face_i, CV_RGB(255, 0,0), 0.8);
            string box_text = format("ID: %d", label);

            //if(label == 1) exibir++;
            //cout << label << " : " << exibir << endl;

            int pos_x = std::max(face_i.tl().x - 10, 0);
            int pos_y = std::max(face_i.tl().y - 10, 0);

            //vector<int> compression_params;
            //compression_params.push_back(CV_IMWRITE_PNG_COMPRESSION);
            //compression_params.push_back(9);
            //imwrite("asdasd.png", face_resized, compression_params);

            putText(original, box_text, Point(pos_x, pos_y), FONT_HERSHEY_PLAIN, 1.0, CV_RGB(255, 0,0), 1.5);
            if(log) {
                for(unsigned int j = 0; j < ids.size(); j++) {
                    if(ids[j] == label) {
                        generate_log(nomes[j], class_name);
                        ids[j] = -1;
                    }
                }
            }
        }

        //if(exibir % 2) 
        	imshow("face_recognizer", original);

        //gettimeofday(&end, NULL);
        //seconds  = end.tv_sec  - start.tv_sec;
        //useconds = end.tv_usec - start.tv_usec;
        //mtime = ((seconds) * 1000 + useconds/1000.0) + 0.5;
        //cout << mtime << " ms" << endl;

        //exibir++;
    }
}

bool generate_log(string nome, string class_name) {
    time_t rawtime;
    struct tm * timeinfo;
    char buffer[80];
	char data[40];
	ofstream myfile;

	time (&rawtime);
	timeinfo = localtime(&rawtime);

    strftime(buffer,80,"%d-%m-%Y %I:%M:%S",timeinfo);
    std::string str(buffer);	

    strftime(data,80,"%d-%m-%Y",timeinfo);
	std::string str2(data);

	class_name = "log/" + class_name + "_log_" + data;

	myfile.open(class_name.c_str(), fstream::app);
	myfile << "N: " << nome << " DH: " << buffer << "\n";
	myfile.close();	
	return false;
}

static void read_csv(const string& filename, vector<Mat>& images, vector<int>& labels, char separator = ';') {
    std::ifstream file(filename.c_str(), ifstream::in);
    if (!file) {
        string error_message = "No valid train filename.";
        CV_Error(CV_StsBadArg, error_message);
    }
    string line, path, classlabel;
    while (getline(file, line)) {
        stringstream liness(line);
        getline(liness, path, separator);
        getline(liness, classlabel);
        if(!path.empty() && !classlabel.empty()) {
            images.push_back(imread(path, 0));
            labels.push_back(atoi(classlabel.c_str()));
        }
    }
}

static void read_class(const string& filename, vector<string>& nomes, vector<int>& ids, char separator = ';') {
    std::ifstream file(filename.c_str(), ifstream::in);
    if (!file) {
        string error_message = "No valid class filename.";
        CV_Error(CV_StsBadArg, error_message);
    }
    string line, id, classlabel;
    while (getline(file, line)) {
        stringstream liness(line);
        getline(liness, id, separator);
        getline(liness, classlabel);
        if(!id.empty() && !classlabel.empty()) {
			ids.push_back(atoi(id.c_str()));
            nomes.push_back(classlabel.c_str());
        }
    }
}

void addClass(){
    int id = 1;
    string aula, pessoa;
    ofstream myfile;

    cout << "Nome da aula: " << endl;
    cin >> aula;
    aula = "class/" + aula;
    myfile.open(aula.c_str(), fstream::app);

    while(true){
        cout <<  "ID e pessoa" << endl;
        scanf("%d", &id);
        if(id == 0) break;
        scanf("%s", &pessoa[0]);
        myfile << id << ";" << pessoa.c_str() << "\n";
    }
    
    myfile.close();
}

void trainFaceRec(int id){
    VideoCapture capture;
    Mat frame;
    bool pic=false;

    if( !face_cascade.load( face_cascade_name ) ){ printf("--(!)Error loading face cascade\n"); return; };
    if( !eyes_cascade.load( eyes_cascade_name ) ){ printf("--(!)Error loading eyes cascade\n"); return; };

    capture.open( -1 );
    if ( ! capture.isOpened() ) { printf("--(!)Error opening video capture\n"); return; }

    while ( capture.read(frame) )
    {
        if( frame.empty() )
        {
            printf(" --(!) No captured frame -- Break!");
            break;
        }

        detectAndDisplay( frame, pic, id );
        pic = false;

        int c = waitKey(10);
        if( (char)c == 27 ) break;
        if( (char)c == 32 ) pic = true;
    }
}

void detectAndDisplay( Mat frame, bool pic, int id )
{
    std::vector<Rect> faces;
    Mat frame_gray;
    vector<int> compression_params;
    int rx, ry, lx, ly;
    string name;

    cvtColor( frame, frame_gray, COLOR_BGR2GRAY );
    //equalizeHist( frame_gray, frame_gray );

    face_cascade.detectMultiScale( frame_gray, faces, 1.1, 2, 0, Size(80, 100) );

    for( size_t i = 0; i < faces.size(); i++ )
    {
        Mat faceROI = frame_gray( faces[i] );
        std::vector<Rect> eyes;
        
        eyes_cascade.detectMultiScale( faceROI, eyes, 1.1, 2, 0 |CASCADE_SCALE_IMAGE, Size(30, 30) );
        if( eyes.size() == 2) { 
            rectangle(frame, faces[i], CV_RGB(255, 255,255), 0.5);
            
            if(pic) {
                if(eyes[0].x < eyes[1].x) {
                    rx = eyes[0].x + eyes[0].width/2;
                    ry = eyes[0].y + eyes[0].height/2;
                    lx = eyes[1].x + eyes[1].width/2;
                    ly = eyes[1].y + eyes[1].height/2;
                } else {
                    lx = eyes[0].x + eyes[0].width/2;
                    ly = eyes[0].y + eyes[0].height/2;
                    rx = eyes[1].x + eyes[1].width/2;
                    ry = eyes[1].y + eyes[1].height/2;
                }
                name = itos(id)+'-'+itos(rx)+'-'+itos(ry)+'-'+itos(lx)+'-'+itos(ly);
                compression_params.push_back(CV_IMWRITE_PNG_COMPRESSION);
                compression_params.push_back(9);
                imwrite("./new/"+name+".png", faceROI, compression_params);
            }
        }
    }
    imshow( window_name, frame );
}

string itos(int a) {
    ostringstream temp;
    temp<<a;
    return temp.str();
}